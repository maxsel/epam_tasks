# Tasks solutions for EPAM's Java Web Development training
* **TaxiStation** - OOP basics
* **CallCenter** - multithreading
* **Composite** - parsing text with regex
* **ParsingWithWeb** - XML parsers + servlet&jsp basics